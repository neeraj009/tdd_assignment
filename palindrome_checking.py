class Read:
    def readFromFile(self,f):
        self.file=f
        infile=open(self.file,"r")
        line=infile.readlines()
        infile.close()
        return line


class Palindrome_checker:
      def check(self,index):
          w = Read().readFromFile("input.txt")
          self.string = w[index].strip()
          self.string = self.string.lower();
          flag=True
          for i in range(0, len(self.string)//2):
            if(self.string[i] != self.string[len(self.string)-i-1]):
                flag = False;
                break;
          return flag
