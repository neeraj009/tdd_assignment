import unittest
from palindrome_checking import Palindrome_checker
from unittest.mock import patch

class beforemethod(unittest.TestCase):
    #this function will work as @before in java
    def setUp(self):
        self.word=Palindrome_checker()
    #this function will work as @after in java
    def tearDown(self):
        self.word=None

class test_palindrome_check(beforemethod):

     #Mocking a class
    @patch("palindrome_checking.Read")
    def test_mock(self,mock_b_constructor):
        mock_b = mock_b_constructor.return_value
        mock_b.readFromFile.return_value =["neeraj","varun"]
        self.assertEqual(self.word.check(1),False)

    def test_empty_string(self):
        self.assertEqual(self.word.check(0), True)

    def test_single_letter(self):
        self.assertEqual(self.word.check(1), True)

    def test_double_letter(self):

        self.assertEqual(self.word.check(2), True)

    def test_odd_length_string(self):

        self.assertEqual(self.word.check(3), True)

    def test_even_length_string(self):

        self.assertEqual(self.word.check(4), True)

if __name__ == '__main__':
    unittest.main()

